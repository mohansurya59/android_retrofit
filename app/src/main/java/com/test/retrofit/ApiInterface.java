package com.test.retrofit;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

public interface ApiInterface {
    //post type request used anotation
    @FormUrlEncoded // annotation used in POST type requests
    @POST("/retrofit/register.php")     // API's endpoints
    public void registration(@retrofit.http.Field("name") String name,
                             @retrofit.http.Field("email") String email,
                             @retrofit.http.Field("password") String password,
                             Callback<SignupResponse> callback);
    // API's endpoints
    @GET("/retrofit/getuser.php")
    public void getUsersList(
            Callback<List<UserListResponse>> callback);
    //UserListResponse is POJO class to get the data from API, In above method we use List<UserListResponse>
    // because the data in our API is starting from JSONArray and callback is used to get the response from
    // api and it will set it in our POJO class
}