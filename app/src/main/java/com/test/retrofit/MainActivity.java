package com.test.retrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {
    SignupResponse signupResponseData;
    Button signup,next;
    EditText Name,Password,Email;

//    RecyclerView recyclerView;
//    List<UserListResponse> userListResponseData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signup=findViewById(R.id.submitbtn);
        Name=findViewById(R.id.username);
        Password=findViewById(R.id.password);
        Email=findViewById(R.id.email);
        next=findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,Show_Record.class);
                startActivity(intent);
//


            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate(Name) && validate(Email) && validate(Password)) {
                    signUp();
                }
            }
        });
    }
//    private void getUserListData(){
//        //progress dialogue
//        final ProgressDialog progressDialog=new ProgressDialog(MainActivity.this);
//        progressDialog.setCancelable(false);
//        progressDialog.setMessage("please wait.... the message are loading");
//        progressDialog.show();
//
//        Api.getClients().getUsersList(new Callback<List<UserListResponse>>() {
//            @Override
//            public void success(List<UserListResponse> userListResponses, Response response) {
//              progressDialog.dismiss();
//              userListResponseData=userListResponses;
//                setDataInRecyclerView();
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
//                progressDialog.dismiss();
//            }
//        });
//
//    }
//    private void setDataInRecyclerView(){
//        // set a LinearLayoutManager with default vertical orientation
//        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(MainActivity.this);
//        recyclerView.setLayoutManager(linearLayoutManager);
//
//        // call the constructor of UsersAdapter to send the reference and data to Adapter
//        UsersAdapter usersAdapter = new UsersAdapter(MainActivity.this, userListResponseData);
//        recyclerView.setAdapter(usersAdapter);
//
//    }


    private boolean validate(EditText editText){
        if(editText.getText().toString().trim().length()>0){
            return true;
        }
        editText.setError("Please Fill This");
        editText.requestFocus();
        return false;
    }
    public void signUp(){
        final ProgressDialog dialog=new ProgressDialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("PLEASE WAIT...");
        dialog.show();

        Api.getClients().registration(Name.getText().toString().trim(),
                Email.getText().toString().trim(),
                Password.getText().toString().trim(),new Callback<SignupResponse>() {
                    @Override
                    public void success(SignupResponse signUpResponse, Response response) {
                        // in this method we will get the response from API
                        dialog.dismiss(); //dismiss progress dialog
                        signupResponseData = signUpResponse;
                        // display the message getting from web api
                        Toast.makeText(MainActivity.this, signUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        // if error occurs in network transaction then we can get the error in this method.
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        dialog.dismiss(); //dismiss progress dialog

                    }
                });
    }
}